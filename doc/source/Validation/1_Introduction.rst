############
Introduction
############

Caelus  is a software library suitable for numerical simulations of problems in Continuum Mechanics (CM), with many applications in Computational Fluid Dynamics (CFD) for a wide range of scientific and engineering applications across commercial and academic environments. Caelus is forked from OpenFOAM, version 2.1.1 released by OpenFOAM Foundation which is mainly a collection of libraries written in C++. Caelus is developed and maintained by Applied CCM Pty Ltd.







