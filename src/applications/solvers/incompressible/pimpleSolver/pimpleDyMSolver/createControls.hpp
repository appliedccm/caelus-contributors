#include "createTimeControls.hpp"

bool correctPhi(pimple.dict().lookupOrDefault("correctPhi", false));

bool checkMeshCourantNo(pimple.dict().lookupOrDefault("checkMeshCourantNo", false));
